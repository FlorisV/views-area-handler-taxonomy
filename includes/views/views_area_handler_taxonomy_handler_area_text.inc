<?php

/**
 * Created by PhpStorm.
 * User: floris
 * Date: 22-02-17
 * Time: 20:13
 */
class views_area_handler_taxonomy_handler_area_text extends views_handler_area {

  function option_definition() {
    $options = parent::option_definition();
    $options['content'] = array('default' => '', 'translatable' => TRUE, 'format_key' => 'format');
    return $options;
  }

  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);

    $form['content'] = array(

      '#type' => 'text_format',
      '#default_value' => $this->options['content'],
      '#rows' => 6,
      '#format' => NULL,
      '#wysiwyg' => FALSE,
    );

  }

  function options_submit(&$form, &$form_state) {
    $form_state['values']['options']['content'] = $form_state['values']['options']['content']['value'];
    parent::options_submit($form, $form_state);
  }

  function render($empty = FALSE) {
    $content = '<h2>test the text rander area</h2>';
    return $content;
  }

}