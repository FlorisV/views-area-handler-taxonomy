<?php

/**
 * Created by PhpStorm.
 * User: floris
 * Date: 16-02-17
 * Time: 16:27
 */
class views_area_handler_taxonomy_handler_area_description extends views_handler_area {

  /**
   * Helper function to get the available vocabulaires for this view header
   */
  function get_available_vocabulaires(){
    $available_vocabulaires_names = array();
    $available_vocabulaires_machine_names = array();
    // find out which vocabulaire (taxonomy) based filters available
    if(!empty($this->view->display_handler->default_display->options['filters'])) {
      foreach ($this->view->display_handler->default_display->options['filters'] as $filter){
        if(!empty($filter['vocabulary'])){
          $vocabulaire = taxonomy_vocabulary_machine_name_load($filter['vocabulary']);
          $available_vocabulaires_objects[$vocabulaire->machine_name] = $vocabulaire;
          $available_vocabulaires_names[$vocabulaire->machine_name] = $vocabulaire->name;
          $available_vocabulaires_ids[$vocabulaire->machine_name] = $vocabulaire->vid;
          $available_vocabulaires_machine_names[$vocabulaire->machine_name] = $vocabulaire->machine_name;
        }
      }
    }

    return array(
      'vids' => $available_vocabulaires_ids,
      'objects' => $available_vocabulaires_objects,
      'names' => $available_vocabulaires_names,
      'machine_names' => $available_vocabulaires_machine_names,
    );
  }

  function option_definition() {
    $options = parent::option_definition();

    $options['terms_title'] = array('default' => NULL);
    $options['vocabulaires'] = array('default' => array());
    $options['max_number_of_terms'] = array('default' => -1);
    $options['max_total_number_of_terms'] = array('default' => -1);
    $options['max_total_results_behaviour'] = array('default' => '');
    $options['exeeded_results_text']['content'] = array('default' => '', 'translatable' => TRUE, 'format_key' => 'format');
    $options['exeeded_results_text']['format'] = array('default' => NULL);
    $options['no_terms_behaviour'] = array('default' => '');
    $options['no_terms_text']['content'] = array('default' => '', 'translatable' => TRUE, 'format_key' => 'format');
    $options['no_terms_text']['format'] = array('default' => NULL);
    $options['hide_dots'] = array('default' => '');
    $options['hide_vocabulaire_names'] = array('default' => '');


    return $options;
  }


  // creat an additional checkbox as option
  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);

    $form['area_handler_description']['markup'] = array(
      '#markup' => '<div class="description area-handler-taxonomy-term">'
        . t('Vocabulaires are shown in the order the filters are in.') . '<br>'
        . t('Terms are shown in the same order as the terms are in the filter')
        . '</div>',
    );

    // show checkboxes to select which vocabulaires should be used
    $this->show_vocabulaire_selection($form, $form_state);

    $form['terms_title'] = array(
      '#type' => 'textfield',
      '#title' => t('Title above the selected terms'),
      '#default_value' => $this->options['terms_title'],
      '#description' => t('when you enter here a title and when there are selected terms, the title will be shown above these terms'),
    );

    $form['max_number_of_terms'] = array(
      '#type' => 'select',
      '#title' => t('Limit number of displayed terms per vocabulaire'),
      '#options' => array(-1 => t('Unlimited')) + drupal_map_assoc(range(1, 10)),
      '#default_value' => $this->options['max_number_of_terms'],
    );
    
    $form['max_total_number_of_terms'] = array(
      '#type' => 'select',
      '#title' => t('Limit number of total displayed terms'),
      '#options' => array(-1 => t('Unlimited')) + drupal_map_assoc(range(1, 10)),
      '#default_value' => $this->options['max_total_number_of_terms'],
    );

    $form['max_total_results_behaviour'] = array(
      '#type' => 'select',
      '#title' => t('Exeeded total terms behaviour'),
      '#options' => array('show_terms' => 'Show terms withing range', 'hide' => 'Hide this view handler', 'default_text' => 'Show default text'),
      '#default_value' => $this->options['max_total_results_behaviour'],
      '#description' => t('Behaviour when there are more terms selected then the configured total limit'),
    );

    $form['exeeded_results_text'] = array(
      '#type' => 'text_format',
      '#title' => t('Exeeded terms text'),
      '#default_value' => $this->options['exeeded_results_text']['value'],
      '#rows' => 6,
      '#format' => isset($this->options['exeeded_results_text']['format']) ? $this->options['exeeded_results_text']['format'] : filter_default_format(),
      '#wysiwyg' => FALSE,
    );

    $form['no_terms_behaviour'] = array(
      '#type' => 'select',
      '#title' => t('No terms behaviour'),
      '#options' => array('hide_terms' => 'Hide term header', 'default_text' => 'Show default text'),
      '#default_value' => $this->options['no_terms_behaviour'],
    );

    $form['no_terms_text'] = array(
      '#type' => 'text_format',
      '#title' => t('Text when there are no terms selected'),
      '#default_value' => $this->options['no_terms_text']['value'],
      '#rows' => 6,
      '#format' => isset($this->options['no_terms_text']['format']) ? $this->options['no_terms_text']['format'] : filter_default_format(),
      '#wysiwyg' => FALSE,
    );

    $form['hide_vocabulaire_names'] = array(
      '#type' => 'checkbox',
      '#title' => t('Hide the names of the vocabulaire above each set of terms'),
      '#default_value' => $this->options['hide_vocabulaire_names'],
    );

    $form['hide_dots'] = array(
      '#type' => 'checkbox',
      '#title' => t('Hide the 3 dot\'s which you see when there are more results then displayed'),
      '#default_value' => $this->options['hide_dots'],
    );
    
  }


  function options_submit(&$form, &$form_state) {
    parent::options_submit($form, $form_state);

    // only save the vocabulaires that are selected, so filter them first
    $filtered_array = array_filter($form_state['input']['options']['vocabulaires']);
    // we use the machine names to submit the values, this is saver than vids
    // in case the vocabulaires and view is migrated (in that case the vid can change, but the machine name not)
    unset($form_state['values']['options']['vocabulaires']);
    foreach($filtered_array as $vocabulaire_machine_name){
      $form_state['values']['options']['vocabulaires'][$vocabulaire_machine_name] = $vocabulaire_machine_name;
    }

  }


  /**
   * Helper function to show the vocabulaire select list
   */
  function show_vocabulaire_selection(&$form, &$form_state){

    $available_vocabulaires = $this->get_available_vocabulaires();

    // let the user select from which vocabulaires the terms should be shown
    $form['vocabulaires'] = array(
      '#type' => 'checkboxes',
      '#title' => t('Vocabulaires to show'),
      '#description' => t('Select from which vocabulaires you want to show the terms.'),
      '#options' => $available_vocabulaires['names'],
      '#default_value' => $this->options['vocabulaires'],
    );

  }

  /**
   * Custom function
   *
   * Retrieve the taxonomy filters where whe should show the terms from.
   *
   * @return array
   *   Returns an array of view filter objects of the vocabulaires that were
   *   selected to show terms from
   */
  function get_the_taxonomy_to_show(){
    // we trow away the vids, they are misleading when vocabulaires are migrated and got another vid
    // it's more save to trust on the machine names of the vocabulaires, so we do that
    $available_vocabulaires = $this->get_available_vocabulaires();
    $vocabulaires_to_show = array_intersect($available_vocabulaires['machine_names'], $this->options['vocabulaires']);

    $taxonomy_filters_to_show_terms_from = array();
    $tids_to_show = array();
    // get the term reference filters
    foreach($this->view->filter as $key => $filter){
      // when the filter definition has a vocabulaire description we know it's a
      // term definition, so we only need these filters
      if (!empty($filter->definition['vocabulary'])
        // and we only want these vocabulaires that we selected in our settings.
        && in_array($filter->definition['vocabulary'], $vocabulaires_to_show)
      ) {
        // retrieve the term ids that are selected in the (exposed) filter
        $tids_to_show = array_merge($tids_to_show, $filter->value);
        $taxonomy_filters_to_show_terms_from[$key] = $filter;
      }
    }

    return array(
      'terms' => taxonomy_term_load_multiple($tids_to_show), // an array of all the taxonomy terms that should be rendered
      'filters' => $taxonomy_filters_to_show_terms_from // an array of the taxonomy (exposed) filters where these terms comes from
    );
  }

  function render($empty = FALSE) {

    if (!$empty || !empty($this->options['empty'])) {
      // an array that contains the following arrays
      // 'tid' a list of all the tids that should be shown
      // 'filters' a list of all taxonomy filters where we should show the terms from
      $taxonomy_to_show = $this->get_the_taxonomy_to_show();
      $terms = $this->get_terms($taxonomy_to_show);

      $content = '';
      // rendering terms when there are terms
      if (!$terms['no_terms']) {
        $content .= $terms['content'];
      }
      // when there are no terms due to exeeding the limit
      else if ($terms['max_total_terms_exeeded']) {
        if ($this->options['max_total_results_behaviour'] == 'default_text') {
          // get the default text for the exeeded total terms
          $format = isset($this->options['exeeded_results_text']['format']) ? $this->options['exeeded_results_text']['format'] : filter_default_format();
          $content .= check_markup($this->options['exeeded_results_text']['value'], $format, '', FALSE);
        }
      }
      // when there are no terms selected at all
      else {
        if ($this->options['no_terms_behaviour'] && $this->options['no_terms_behaviour'] == 'default_text') {
          // get the default text for the no results
          $format = isset($this->options['no_terms_text']['format']) ? $this->options['no_terms_text']['format'] : filter_default_format();
          $content .= check_markup($this->options['no_terms_text']['value'], $format, '', FALSE);
        }
      }

      if (!empty($content)) {
        $content = '<div class="vocabulaire-wrapper">' . $content . '</div>';
      }

      return $content;
    }

    // when the search result is empty and the area shouldn't be shown on an empty view
    // then just return an empty string.
    return '';
  }

  /**
   * Helper function
   */
  function max_terms_exeeded($i){
    if (!empty($this->options['max_number_of_terms'])
      && -1 != $this->options['max_number_of_terms'] // ignore when max terms is -1 (unlimited)
      && $i > $this->options['max_number_of_terms']
    ) {
      return TRUE;
    }
    return FALSE;
  }

  /**
   * Helper function
   */
  function max_total_terms_exeeded($i_tot){
    if (!empty($this->options['max_total_number_of_terms'])
      && -1 != $this->options['max_total_number_of_terms'] // ignore whem ax terms is -1 (unlimited)
      && $i_tot > $this->options['max_total_number_of_terms']
    ) {
      return TRUE;
    }
    return FALSE;
  }

  /**
   * Helper function to get the terms as html and some settings about the terms.
   */
  function get_terms($taxonomy_to_show){
    $no_terms = TRUE; // at start we have no terms
    $delete_terms_content = FALSE; // a flag to check if we need the terms content

    // loop trough the filters (which are also the vocabulaires)
    $i_tot = 0;
    $max_total_terms_exeeded = FALSE;
    $term_content = '';
    foreach($taxonomy_to_show['filters'] as $taxonomy_filter){
      if(!empty($taxonomy_filter->value)){
        $no_terms = FALSE; //when at least one vocabulaire has a filled value array we have terms
        $term_content .= '<div class="terms-wrapper">';
        if(isset($this->options['hide_vocabulaire_names']) && $this->options['hide_vocabulaire_names'] != 1){
          $term_content .= '<h3>' . $taxonomy_filter->definition['title short'] . '</h3>';
        }

        // loop through the terms
        $i = 0;
        $max_terms_exeeded = FALSE;
        foreach($taxonomy_filter->value as $tid){
          $i++;
          // when max terms per vocabulaire are exeeded
          if($this->max_terms_exeeded($i)){
            // end with a dotdotdot if this is the last term due to the limit.
            if(isset($this->options['hide_dots']) && $this->options['hide_dots'] != 1){
              $term_content .= '<span class="terms-ended-by-limit">...</span>';
              $dots_added = TRUE;
            }

            $max_terms_exeeded = TRUE;
          } else {
            $i_tot++;
          }
          // when max terms in total are exeeded
          if($this->max_total_terms_exeeded($i_tot)){
            // when max total terms exeeded and there is a configuration for max total terms behaviour which isn't 'show terms' flag $delete_terms;
            if(!empty($this->options['max_total_results_behaviour']) && $this->options['max_total_results_behaviour'] != 'show_terms'){
              $delete_terms_content = TRUE;
            } else if(!$dots_added){ // prevent two sets of dot's behind each other
              $term_content .= '<span class="terms-ended-by-limit">...</span>';
            }

            $max_total_terms_exeeded = TRUE;
          }
          if($max_terms_exeeded || $max_total_terms_exeeded){
            break;
          }

          // fetch the term and fill the $term_content variable
          $taxonomy_render_array = taxonomy_term_view($taxonomy_to_show['terms'][$tid], 'views_area_handler');
          $term_content .= '<div class="term_row term_row_' . $tid . '">';
          $term_content .= drupal_render($taxonomy_render_array);
          $term_content .= '</div>';

          $dots_added = FALSE;
        }

        $term_content .= '</div>';

        // breakout vocabulaire loop when max total terms is reached or when we are nog going to use the terms
        if($max_total_terms_exeeded) {
          break;
        }
      }
    }

    if($delete_terms_content){
      // based on configuration and fetched terms it can be that we don't want to show
      // the terms, in that case make $term_content an empty string
      $term_content = '';
      $no_terms = TRUE;
    } else if(!$no_terms) {
      // add the title when there is one configured
      if(!empty($this->options['terms_title'])){
        $term_content = '<h2>' .  $this->options['terms_title'] . '</h2>' . $term_content;
      }
    }

    return array(
      'content' => $term_content,
      'no_terms' => $no_terms,
      'max_total_terms_exeeded' => $max_total_terms_exeeded,
    );
  }
}





//@todo additional functionality we'd like to see
// - create readme
// - test and adjust mulitlingual behaviour
// - test export in views and in features
// - select the view mode that should be used
// - work with a tpl or theme function so the theme can be changed
