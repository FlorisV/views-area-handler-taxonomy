<?php

/**
 * Implements hook_views_data_alter().
 */
function views_area_handler_taxonomy_views_data_alter(&$data){
  $data['views']['taxonomy_area_handler'] = array(
    'title' => t('Taxonomy based on filter'),
    'help' => t('Shows taxonomy description based on an exposed filter'),
    'area' => array(
      'handler' => 'views_area_handler_taxonomy_handler_area_description',
    ),
  );

  $data['views']['taxonomy_area_handler_text'] = array(
    'title' => t('Taxonomy text test area'),
    'help' => t('Just some configurable text to test'),
    'area' => array(
      'handler' => 'views_area_handler_taxonomy_handler_area_text',
    ),
  );
}