-- SUMMARY --
This module provides a taxonomy view_mode for the header or footer of a view.

The term or terms that you're able to see are related to the taxonomy filters for
the view. This can be as wel exposed taxonomy filters as normal taxonomy filters.

Configuration of what information you see from the term, like description or other
fields is done under 'manage display' of your taxonomy vocabulaire. This module
provides therefore the view mode 'Views area handler'.

In the settings of this area handler you can configure
 - from which filters/vocabulaires you expose terms
 - the maximum number of terms to expose per vocabulaire
 - the maximum of terms to expose in general
 - an optional custom text when there are no terms selected
 - an optional custom text when there are more terms selected then the limit

-- WIP --
The basics of the module are working.
Further tesing and development needs to be done on:
 - multilanguage
 - drupal features
 - port to drupal 8
So keep in mind that the module could be not stable enough for your purposes.
If you have remarks or suggestions for any improvements, please let me know.

-- REQUIREMENTS --
 - Views

-- INSTALLATION --
Download this module and enable it like any other modules

-- CONFIGURATION --
 1) Create a view with at least 1 taxonomy filter.
 2) In the view, for the FOOTER or HEADER section click on 'add'
 3) Search for 'Taxonomy based on filter', select it and apply.
 4) Select at least 1 Vocabulaire to show
 5) Save your settings, save your view.

 6) The rendered term uses the view mode 'Views area handler'
 7) So go to your term('s)
 8) Enable the view mode 'Views area handler' under Custom display settings
 9) Select this view mode 'Views area handler'
 10) Configure the fields that you want to see in this view mode.

 11) Now you'r ready. When you take a look at your view and filter on a term you will
     see your term as configured in your header or footer.

 12) The are more options (see customization) so when you want to limit the number of
 rendered terms or want a default text. Please feel free to explore and try the settings.

-- CUSTOMIZATION --
 Vocabulaires to show
   Select for which taxonomy filter / vocabulaire you want to show the terms
 Title above the selected terms
   If you fill in a title here you'll see that above the selected terms
 Limit number of displayed terms per vocabulaire
   Configure the number of maximum terms that you want to display per vocabulaire
 Limit number of total displayed terms
   Configure the number of maximum terms that you want to see in total
 Exeeded total terms behaviour
   Select what the behaviour should be when the maximum total terms are exeeded.
     - Show terms within range, so show the terms limited by your configuration
     - Hide the view handler, so show nothing
     - Show default text, this is the text that you edit under 'Exeeded terms text'
 Exeeded terms text
   Used when 'Show default text' is configured under 'Exeeded total terms behaviour'
 No terms behaviour
   Select what the behaviour should be when there are no terms selected
     - Show default text, this is the text that you edit under 'Text when there are no terms selected'
     - Hide term header
 Text when there are no terms selected
   Used when 'Show default text' is configured under 'No terms behaviour'
 Hide the names of the vocabulaire above each set of terms
   Normally the name of the vocabulaire is shown above each set of terms, select this checkbox to hide
   the name of the vocabulaire
 Hide the 3 dot's which you see when there are more results then displayed
   Normally there are 3 dot's ... displayed when there are more results then displayed (caused
   by limiting the terms per vocabulaire and/or in total)
   Select this checkbox to hide the dots

-- TYPICAL CONFIGURATION TO SHOW ONLY ONE TERM FOR ONE VOCABULAIR --
  Follow the normal configuration steps
  Select only one vocabulaire to show a term from
  Limit number of terms and total terms to 1
  Hide the names of the vocabulaires
  optional: disable the dotdotdot
  optional: provide the same standard text for the case when the selected terms
  exeed the limit or when there are no results. You may want to use a general text
  that tells something about the terms in general or about the results in general.
  Providing this text can help to give general information end/or keep your styling and
  aligning clean wheter there are terms, to much terms or no terms selected.

-- TROUBLESHOOTING --
-- FAQ --